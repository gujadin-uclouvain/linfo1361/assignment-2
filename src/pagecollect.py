from search import *
#import time


#################
# Problem class #
#################
class PageCollect(Problem):

    def __init__(self, initial):
        super().__init__(initial)

    def actions(self, state):
        """
        The actions are not choose if the student faces to a wall (#) in the direction he/she wants to go
        """
        student = state.student
        actions = []
        if state.grid[student[0]][student[1] + 1] != '#':
            actions.append('R')
        if state.grid[student[0]][student[1] - 1] != '#':
            actions.append('L')
        if state.grid[student[0] - 1][student[1]] != '#':
            actions.append('U')
        if state.grid[student[0] + 1][student[1]] != '#':
            actions.append('D')
        return actions
    
    def result(self, state, action):
        """
        Change the student position,
        If a page is taken, it's position will be removed
        Update the grid
        """
        new_student, new_pages = list(state.student), list(state.pages)
        if action == 'R': new_student[1] += 1
        elif action == 'L': new_student[1] -= 1
        elif action == 'U': new_student[0] -= 1
        elif action == 'D': new_student[0] += 1
        new_student = tuple(new_student)

        for id_page, page in enumerate(state.pages):
            if new_student == page:
                new_pages.pop(id_page)
                break
        new_pages = tuple(new_pages)

        return State(PageCollect.__update_grid(state, new_student), new_student, state.teacher, new_pages)

    @staticmethod
    def __update_grid(state, new_student):
        """
        Update the grid when the student moves
        """
        new_grid = [line.copy() for line in state.grid]
        # Update Player
        if state.student == state.teacher:
            new_grid[state.student[0]][state.student[1]] = 'X'
        else:
            new_grid[state.student[0]][state.student[1]] = ' '
        new_grid[new_student[0]][new_student[1]] = '@'
        return new_grid

    def goal_test(self, state):
        """
        There is no pages on the grid and the student is at the same place as the teacher.
        """
        return state.student == state.teacher and len(state.pages) == 0

    def h(self, node):
        """
        Using the Manhattan distance between each pages.
        If all pages are alreaady taken, use the algorithm to reach the teacher.
        """
        state = node.state
        if len(state.pages) == 0:
            return PageCollect.__manhattan_distance(state.student, state.teacher)

        else:
            min_val = float("inf")
            for page in state.pages:
                min_val = min(min_val, PageCollect.__manhattan_distance(state.student, page))
            return min_val

    @staticmethod
    def __manhattan_distance(a, b):
        """
        Basic formula of the Manhattan distance
        """
        return abs(a[0] - b[0]) + abs(a[1] - b[1])

    @staticmethod
    def load(path):
        with open(path, 'r') as f:
            lines = f.readlines()
            
        state = State.from_string(''.join(lines))
        return PageCollect(state)


###############
# State class #
###############
class State:
    def __init__(self, grid, student, teacher, pages):
        self.nbr = len(grid)
        self.nbc = len(grid[0])
        self.grid = grid
        self.student = student       # (x, y)
        self.teacher = teacher       # (x, y)
        self.pages = pages           # ((x, y), (x, y), ...)

    def __str__(self):
        return '\n'.join(''.join(row) for row in self.grid)

    def __eq__(self, other):
        """
        Check the location of the students and the number of pages taken.
        """
        if self.nbr == other.nbr and self.nbc == self.nbc:
            if self.student == other.student:
                if len(self.pages) == len(other.pages):
                    return True
        return False

    def __hash__(self):
        return hash((self.nbr, self.nbc, self.student, self.pages))
    
    def __lt__(self, other):
        return hash(self) < hash(other)

    @staticmethod
    def from_string(string):
        lines = string.strip().splitlines()
        grid = list(map(lambda x: list(x.strip()), lines))
        # Find the student, teacher and all pages positions on the grid
        student, teacher, pages = None, None, []
        for row in range(1, len(grid)-1):
            for col in range(1, len(grid[0])-1):
                if grid[row][col] == '@':
                    student = (row, col)
                elif grid[row][col] == 'X':
                    teacher = (row, col)
                elif grid[row][col] == 'p':
                    pages.append((row, col))
        return State(grid, student, teacher, tuple(pages))


#####################
# Launch the search #
#####################
problem = PageCollect.load(sys.argv[1])

# start_timer = time.perf_counter()
node = astar_search(problem, display=False)
# node = breadth_first_graph_search(problem, display=True)
# end_timer = time.perf_counter()

# example of print
path = node.path()

# print("Execution time: ", str(end_timer - start_timer))
print("Number of moves: ", node.depth)
for n in path:
    print(n.state)  # assuming that the __str__ function of state outputs the correct format
    print()
