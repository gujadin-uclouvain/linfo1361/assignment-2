[LINFO1361] IA - Assignment n°2
---

**Membres:**

* Guillaume Jadin
* Thomas Robert


**Description:**

* Upload your best PageCollect solver ! It must solve the problem to optimality, that is reaching the goal state with a MINIMUM NUMBER OF MOVES.

* Your file must have the extension .py and satisfy the output format specified in the assignment description on Moodle.

* Your solver will be tested against 13 instances: 10 instances are available to you with the assignment, the last three are hidden.

* For each instance, your solver will have a maximum of 60 seconds in order to solve it to optimality. If the time limit is exceeded, or if the solution is not optimal, the instance is failed.


**Restrictions:**

* pagecollect.py (INGInious): Contains your Python 3 implementation of the Pacmen problem solver. Your program should take the path to the instance files as only argument. The search strategy that should be enabled by default in your programs is A∗ with your best heuristic. Your program should print the solution to the standard output in the format described further. The file must be encoded in utf-8.

* report_A2_group_XX.pdf (Gradescope): Answers to all the questions using the provided template. Remember, the more concise the answers, the better.


**INGInious:**

* https://inginious.info.ucl.ac.be/course/LINGI2261/2022_A2_pagecollect


**Overleaf:**

* https://www.overleaf.com/9692426681nzbxjpwxfhsx